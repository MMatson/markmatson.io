import React, {useCallback, useEffect, useRef, useState} from "react";


export const useDarkLightModeToggle = (initialState: boolean) => {
    const [isDark, setIsDark] = useState(initialState);
    const isDarkRef = useRef(isDark);

    const toggle = useCallback(
        () => setIsDark(!isDarkRef.current),
        [isDarkRef, setIsDark]
    );

    useEffect(
        () => {
            isDarkRef.current = isDark;
        },
        [isDark]
    );

    return [isDark, toggle];
}
