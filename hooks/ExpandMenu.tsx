import React, {useCallback, useEffect, useRef, useState} from "react";


export const useMenuToggle = (initialState: boolean) => {
    const [isExpanded, setExpanded] = useState(initialState);
    const isExpandedRef = useRef(isExpanded);

    const toggle = useCallback(
        () => setExpanded(!isExpandedRef.current),
        [isExpandedRef, setExpanded]
    );

    useEffect(
        () => {
            isExpandedRef.current = isExpanded;
        },
        [isExpanded]
    );

    return [isExpanded, toggle];
}
