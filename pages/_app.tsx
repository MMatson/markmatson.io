import '../styles/globals.css'
import {AppProps} from "next/app";
import {useDarkLightModeToggle} from "../hooks/DarkMode";
import React from "react";
import "@fortawesome/fontawesome-svg-core/styles.css";

export default function MyApp({ Component, pageProps }: AppProps) {
  const [darkMode, toggleDarkMode] = useDarkLightModeToggle(true);

  return (
      <Component darkMode={darkMode} toggleDarkMode={toggleDarkMode} {...pageProps} />
  )
}
