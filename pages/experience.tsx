import PageLayout from "../components/PageLayout";
import React from "react";
import Card from "../components/Card";
import styles from "../styles/Home.module.css";


type Props = {
    darkMode: boolean;
    toggleDarkMode: (() => void);
}

export default function Experience({ darkMode, toggleDarkMode }: Props) {
    return (
        <PageLayout darkMode={darkMode} toggleDarkMode={toggleDarkMode} title={"MM | Experience"}>
            <h1>Roles</h1>
            <Card title={"Data Specialist, Investment Ops"} darkMode={darkMode}>
                <h2><i>BNY Mellon | May 2019 to Present</i></h2>
                <p>
                    • Managed operational efficiency model for a department of 250 employees globally
                    in order to perform cost and productivity analysis as well as project labor requirements
                </p>
                <p>
                    • Automated investment accounting Excel workflows for functions such as:
                    Trade Processing (Equity/Fixed Income),
                    Net Asset Value (NAV) Calculation, and
                    Mutual Fund Price Calculation/Verification
                </p>
                <p>
                    • Acted as the department's subject matter expert on Excel and SharePoint, conducting training and classes for individuals and teams of all skill levels
                </p>
            </Card>

            <Card title={"Lead Analyst, Portfolio Accounting"} darkMode={darkMode}>
                <h2><i>BNY Mellon | March 2018 to April 2019</i></h2>
                <p>
                    • Lead a small team of analysts in providing ad-hoc accounting calculation, analysis, and reporting services to clients, while also developing scripts for automation of processes
                </p>
            </Card>

            <Card title={"Senior Analyst, Portfolio Accounting"} darkMode={darkMode}>
                <h2><i>BNY Mellon | February 2017 to February 2018</i></h2>
                <p>
                    • Provided fund accounting reconciliation and reporting analysis for a variety of client investment accounts and strategies, including equity, fixed-income, and derivatives
                </p>
            </Card>

            <Card title={"Projection Manager"} darkMode={darkMode}>
                <h2><i>AMC Theatres | May 2012 to February 2017</i></h2>
                <p>
                    • Managed projection systems and facilities, including inventory management, schedule optimization, and technical troubleshooting and repair of computers and equipment
                </p>
            </Card>

            <h1>Education</h1>
            <Card title={"Master of Software Engineering"} darkMode={darkMode}>
                <h2><i>Penn State University, 2020</i></h2>
            </Card>

            <Card title={"Bachelor of Science in Economics"} darkMode={darkMode}>
                <h2><i>University of Pittsburgh, 2016</i></h2>
            </Card>

            <h1>Technologies</h1>
            <Card title={""} darkMode={darkMode}>
                <div className={styles.columns}>
                    <div className={styles.column}>
                        <p>• Swift</p>
                        <p>• TypeScript/JS</p>
                        <p>• HTML/CSS</p>
                        <p>• Python</p>
                        <p>• Go</p>
                    </div>
                    <div className={styles.column}>
                        <p>• Angular</p>
                        <p>• React</p>
                        <p>• Next.js</p>
                        <p>• SwiftUI</p>
                        <p>• PyTorch</p>
                    </div>
                    <div className={styles.column}>
                        <p>• Gin</p>
                        <p>• MongoDB</p>
                        <p>• SQL</p>
                        <p>• Java</p>
                        <p>• Visual Basic</p>
                    </div>
                </div>
            </Card>
        </PageLayout>
    )
}