import styles from '../styles/Home.module.css'
import PageLayout from "../components/PageLayout";
import Card from "../components/Card";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faExternalLinkAlt} from "@fortawesome/free-solid-svg-icons";
import React from "react";
import Link from "next/link";


type Props = {
    darkMode: boolean;
    toggleDarkMode: (() => void);
}

export default function Home({ darkMode, toggleDarkMode }: Props) {
    return (
        <PageLayout darkMode={darkMode} toggleDarkMode={toggleDarkMode} title={"MM | Mark Matson"}>
            <h1>Welcome</h1>

            <Card title={"About"} darkMode={darkMode}>
                <p>
                    Software developer and data analyst.
                </p>
                <p>
                    Experience with frontend & backend web (Angular, React, Go), mobile (iOS/Swift), and some other technologies (e.g. Python, Java).
                </p>
                <p>
                    Excel expert.
                </p>
                <p>
                    Currently working on learning ML/DL fundamentals for computer vision applications.
                </p>
                <p>
                    I also like brewing beer.
                </p>
            </Card>

            <Card title={"Latest Project"} darkMode={darkMode}>
                <p>
                    <Link href={"/projects"}>
                        <a>CNN for Recognizing Handwritten Numbers{" "}<FontAwesomeIcon icon={faExternalLinkAlt}/></a>
                    </Link>
                </p>
                <div className={styles.gifBlock}>
                    <img className={styles.gif} src="gif1.gif" alt={"first gif of app"}/>
                    <img className={styles.gif} src="gif2.gif" alt={"second gif of app"}/>
                    <img className={styles.gif} src="gif3.gif" alt={"third gif of app"}/>
                </div>
            </Card>
        </PageLayout>
    )
}
