import Card from "../components/Card";
import PageLayout from "../components/PageLayout";
import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faExternalLinkAlt} from "@fortawesome/free-solid-svg-icons";
import styles from "../styles/Home.module.css";


type Props = {
    darkMode: boolean;
    toggleDarkMode: (() => void);
}


export default function Projects({ darkMode, toggleDarkMode }: Props) {
    let codeStyle = darkMode ? styles.darkCode : styles.lightCode;
    let siteImage = darkMode ? "img2.png" : "img1.png";

    return (
        <PageLayout darkMode={darkMode} toggleDarkMode={toggleDarkMode} title={"MM | Projects"}>
            <h1>Projects</h1>

            <Card darkMode={darkMode} title={'Digit Reader ConvNet'}>
                <h2><i>PyTorch, Swift, Objective-C</i></h2>
                <div className={styles.gifBlock}>
                    <img className={styles.gif} src="gif1.gif" alt={"first gif of app"}/>
                    <img className={styles.gif} src="gif2.gif" alt={"second gif of app"}/>
                    <img className={styles.gif} src="gif3.gif" alt={"third gif of app"}/>
                </div>
                <p>
                    <br/>
                    This is my "hello world" machine learning/computer vision project: a simple convolutional neural network trained in PyTorch on the MNIST dataset.
                    I wanted to be able to test the model on my own input, so I built an iOS app where I can draw numbers on the screen and see how well it can infer my drawings.
                </p>
                <p>
                    The network has gone through a couple of iterations in order to get to the above performance.
                    It was relatively easy to achieve high accuracy in a couple of epochs of training with earlier iterations of the architecture,
                    but refinement and normalization to correct overfitting were required for it work acceptably on the real world input.
                </p>
                <p>
                    Below are the final network layers I used. See the source code for hyperparameters - it takes two 2/3 epochs to converge to about 99% accuracy on the MNIST validation set.
                </p>
                <div className={codeStyle}>
                    Test Set: Average Loss: 0.0004, Accuracy: 9924/10000 (99%)
                </div>
                <div className={codeStyle}>
                    MNIST_CNN(<br/>
                    &emsp;(convolutional_layers): Sequential(<br/>
                    &emsp;&emsp;(0): Conv2d(1, 32, kernel_size=(5, 5), stride=(1, 1))<br/>
                    &emsp;&emsp;(1): ReLU()<br/>
                    &emsp;&emsp;(2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)<br/>
                    &emsp;&emsp;(3): Conv2d(32, 64, kernel_size=(5, 5), stride=(1, 1))<br/>
                    &emsp;&emsp;(4): ReLU()<br/>
                    &emsp;&emsp;(5): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)<br/>
                    &emsp;)<br/>
                    &emsp;(linear_layers): Sequential(<br/>
                    &emsp;&emsp;(0): Linear(in_features=1024, out_features=1024, bias=True)<br/>
                    &emsp;&emsp;(1): ReLU()<br/>
                    &emsp;&emsp;(2): Dropout(p=0.2, inplace=False)<br/>
                    &emsp;&emsp;(3): Linear(in_features=1024, out_features=10, bias=True)<br/>
                    &emsp;)<br/>
                    )<br/>
                </div>
                <p>
                    <a href={"https://gitlab.com/MMatson/pytorchmnist"}>
                        Convolutional Neural Net/Trainer Source{" "}<FontAwesomeIcon icon={faExternalLinkAlt}/>
                    </a>
                </p>
                <p>
                    <a href={"https://gitlab.com/MMatson/NeuralNets"}>
                        iOS Test App Source{" "}<FontAwesomeIcon icon={faExternalLinkAlt}/>
                    </a>
                </p>
            </Card>

            <Card darkMode={darkMode} title={"Music Streaming App"}>
                <h2><i>Angular, TypeScript, Go, MongoDB</i></h2>
                <img className={styles.image} src="gif4.gif" alt={"gif of app"}/>
                <p>
                    Lion was a music streaming web application that I developed for my Master's Capstone project.
                    The project team consisted of 5 members: a scrum master, two developers, and two on testing/documentation.
                </p>
                <p>
                    I acted as the lead developer/architect for the application, as well as the database admin.
                    The project started with me setting up the all of the endpoints for account management and MP3 file serving/streaming on the backend in sprint 1,
                    completing the core backend functionality; however, frontend development was stalled.
                    I was asked to take over frontend development, and recreated a new application from the ground up, and moved to that version going forward.
                    This allowed me to develop a functional application by the end of sprint 2, and accelerate the feature set development in sprint 3.
                </p>
                <p>
                    <a href={"https://gitlab.com/capstone894group5/frontend"}>
                        Frontend Source{" "}<FontAwesomeIcon icon={faExternalLinkAlt}/>
                    </a>
                </p>
                <p>
                    <a href={"https://gitlab.com/capstone894group5/backend"}>
                        Backend Source{" "}<FontAwesomeIcon icon={faExternalLinkAlt}/>
                    </a>
                </p>
            </Card>

            <Card darkMode={darkMode} title={"Mark Matson.io"}>
                <h2><i>Next.js, React, TypeScript</i></h2>
                <img className={styles.image} src={siteImage} alt={"image of site"}/>
                <p>For this site I wanted to get exposure to another TS/JS framework (React). I also wanted to do the CSS, dark mode, etc. myself, which means it's... probably not the best.</p>
                <p>
                    <a href={""}>
                        Source{" "}<FontAwesomeIcon icon={faExternalLinkAlt}/>
                    </a>
                </p>
            </Card>
        </PageLayout>
    )
}