import {ReactNode} from "react";
import styles from "../styles/Home.module.css";
import Head from "next/head";
import NavigationBar from "./NavigationBar";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faLinkedin, faGitlab} from "@fortawesome/free-brands-svg-icons";
import {faEnvelope} from "@fortawesome/free-solid-svg-icons";

type Props = {
    darkMode: boolean;
    toggleDarkMode: (() => void);
    children?: ReactNode;
    title?: string;
}

export default function PageLayout({ darkMode, toggleDarkMode, children, title = "Mark Matson.io"}: Props) {
    let containerStyle = darkMode ? styles.darkContainer : styles.lightContainer;
    let footerStyle = darkMode ? styles.darkFooter : styles.lightFooter;
    let iconStyle = darkMode ? styles.darkFooterIcons : styles.lightFooterIcons;

    return (
        <div className={containerStyle}>
            <Head>
                <title>{title}</title>
                <link rel="icon" href="favicon.ico" />
                <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Archivo|IBM+Plex+Mono:Medium500"/>
            </Head>

            <NavigationBar darkMode={darkMode} toggleDarkMode={toggleDarkMode}/>

            <main className={styles.main}>
                {children}
            </main>

            <footer className={footerStyle}>
                <div className={iconStyle}>
                    <a href={"https://gitlab.com/MMatson"}>
                        <h2>
                            <FontAwesomeIcon icon={faGitlab}/>
                        </h2>
                    </a>
                    <a href={"https://www.linkedin.com/in/matsonm"}>
                        <h2>
                            <FontAwesomeIcon icon={faLinkedin}/>
                        </h2>
                    </a>
                    <a href={"mailto:m.matson@pitt.edu"}>
                        <h2>
                            <FontAwesomeIcon icon={faEnvelope}/>
                        </h2>
                    </a>
                </div>
            </footer>
        </div>
    )
}