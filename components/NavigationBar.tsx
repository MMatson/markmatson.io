import styles from "../styles/Home.module.css";
import Link from "next/link";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSun, faMoon, faBars, faWindowClose} from "@fortawesome/free-solid-svg-icons";
import React from "react";
import {useMenuToggle} from "../hooks/ExpandMenu";


type NavigationBarProps = {
    darkMode: boolean;
    toggleDarkMode: (() => void);
}


export default function NavigationBar({ darkMode, toggleDarkMode}: NavigationBarProps) {
    let [menuExpanded, toggleMenu] = useMenuToggle(false);

    const toggleMobileMenu = () => {
        let toggleFunc = toggleMenu as (() => void);
        toggleFunc();
    }

    const toggleDisplayMode = () => {
        toggleDarkMode();
    }

    let navigationStyle = darkMode ? styles.darkNavigation : styles.lightNavigation;
    let logoStyle = darkMode ? styles.darkNavigationLogo : styles.lightNavigationLogo;
    let modeToggleStyle = darkMode ? styles.darkDarkModeToggle : styles.lightDarkModeToggle;
    let darkModeIcon = darkMode ? faSun : faMoon;

    let icon = menuExpanded ? <FontAwesomeIcon icon={faWindowClose} onClick={toggleMobileMenu}/> : <FontAwesomeIcon icon={faBars} onClick={toggleMobileMenu}/>;
    let navLinkStyle = menuExpanded ? styles.mobileNavigationLinks : styles.navigationLinks;
    let menuStyle = menuExpanded ? styles.mobileMenuClose : styles.mobileMenuOpen;

    return (
        <nav className={navigationStyle}>
            <div className={navLinkStyle}>
                <div className={logoStyle}>
                    <Link href={"/"}>
                        <h2>Mark Matson.io</h2>
                    </Link>
                </div>
                <Link href="/">
                    <a>
                        <h2>Home</h2>
                    </a>
                </Link>
                <Link href="/projects">
                    <a>
                        <h2>Projects</h2>
                    </a>
                </Link>
                <Link href="/experience">
                    <a>
                        <h2>Experience</h2>
                    </a>
                </Link>
                <div className={modeToggleStyle}>
                     <a>
                        <h2>
                            <FontAwesomeIcon icon={darkModeIcon} onClick={toggleDisplayMode}/>
                        </h2>
                    </a>
                </div>
                <div className={menuStyle}>
                    <h2>
                        {icon}
                    </h2>
                </div>
            </div>
        </nav>
    )

}


