import styles from "../styles/Home.module.css";

import {ReactNode} from "react";

type Props = {
    title: string;
    darkMode: boolean;
    children?: ReactNode;
}


export default function Card({ title, darkMode, children}: Props) {
    let cardStyle = darkMode ? styles.darkCard : styles.lightCard;
    let titleStyle = darkMode ? styles.darkCardTitle : styles.lightCardTitle;

    return (
        <div className={cardStyle}>
            <h3 className={titleStyle}>{title}</h3>
            {children}
            <p></p>
        </div>
    );
}